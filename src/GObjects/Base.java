package GObjects;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import GSystem.Game;

public class Base extends GameObjects{
BufferedImage base;
Engine engine;	
	Game game;
	public Base(int x, int y, ID id,Engine engine,Game game) {
		super(x, y, id,ResourceLoader.getImage("base.png"));
		this.engine = engine;
		this.game = game;
		engine.addObject(new HUDB(0,350,ID.HUDB,engine));
	}

	@Override
	public void tick() {
		collision();
		if(HUDB.HEALTH<=0)
		{
			System.out.println("You Lose!");
			game.stop();
		}
		
		if(HUD.HEALTH<=0)
		{
			System.out.println("Beeeng You Win!");
			game.stop();
		}
	}

	private void collision(){
		for(int i=0;	i<engine.gameobjects.size();	i++){
			GameObjects tempObject = engine.gameobjects.get(i);
			
			if(tempObject.getId() == ID.EnemyShoot){
				if(getBounds().intersects(tempObject.getBounds())){
					HUDB.HEALTH--;
				}
			}
		}
	}
	
	
	@Override
	public void render(Graphics g) {
		g.drawImage(image, (int)X,(int)Y,null);
		
	}

	@Override
	public Rectangle getBounds() {
		// TODO Auto-generated method stub
		return new Rectangle(0,390,100,100);
	}

}
