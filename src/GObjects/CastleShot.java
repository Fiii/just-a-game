package GObjects;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class CastleShot extends GameObjects {
	
	Engine engine;
	double v0,posx,posy,time,vx,vy,dt;
	double angle,rad,dab;
	boolean shot = false;

	
	public CastleShot(int x, int y, ID id,Engine engine) {
		super(x, y, id,null);
		this.engine = engine;

		v0 =100;
		angle = 1;
		dt = 0.1; // s

		vx = v0 * Math.cos(Math.PI / 180 * angle);
		vy = v0 * Math.sin(Math.PI / 180 * angle); 

		X = 850; // m
		Y =  250;  // m
		time = 1; // s
	}

	@Override
	public void tick() {
		
		if(true){
		
		   X -=  vx * dt;
		   Y -= vy * dt;
		   time += dt;
		   vy -= 9.82 * dt;
		}		
		   if(X<=-50||Y>=500){
			   shot = false;
			   
				v0 =100;
				angle = 1;
				vx = v0 * Math.cos(Math.PI / 180 * angle);
				vy = v0 * Math.sin(Math.PI / 180 * angle); 
				dt = 0.1; // s
				
				 vy -= 9.82 * dt;
				X =  850; // m
				Y =  250; // m
				time =1; // s
		
		   }
		   collision();
	}

	public void render(Graphics g) {
		g.setColor(Color.red);
		g.fillOval((int)X+18,(int)Y+3, 10, 10);
	}
	
	private void collision(){
		for(int i=0;	i<engine.gameobjects.size();	i++){
			GameObjects tempObject = engine.gameobjects.get(i);
			
			if(tempObject.getId() == ID.Shot){
				//collision code
				if(getBounds().intersects(tempObject.getBounds())){
					//collision code 
					engine.removeObject(this);
					//engine.removeObject(tempObject);
				    engine.addObject(new CastleShot(30,500,ID.CastleShot,engine));
				    
				}
			}
		}
	}
	
	
	@Override
	public Rectangle getBounds() {
		// TODO Auto-generated method stub
		return new Rectangle((int)X,(int)Y,25,25);
	}
}
