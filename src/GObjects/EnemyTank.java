package GObjects;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class EnemyTank extends Enemy {

	public EnemyTank(int x, int y, ID id, Engine engine) {
		super(x, y, id, engine,ResourceLoader.getImage("enemytank.png"));
		setMoveXSpeed(1);
		setMoveYSpeed(5);
	}
	
	public Rectangle getBounds() {
		return new Rectangle((int)X,(int)Y,30,30);
	}
	
}
