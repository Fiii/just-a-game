package GObjects;


public enum ID{ 

 
     Player(),
     Shot(),
     CastleShot(),
     Castle(),
     Base(),
     HUD(),
     HUDB(),
     EnemyHud(),
     Enemy(),
     BG(),
     Trail(),
     EnemyShoot(),
     SPHUD(),
     Ball();
     
}
