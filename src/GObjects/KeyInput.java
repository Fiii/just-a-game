package GObjects;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;



public class KeyInput extends KeyAdapter {
private Engine engine;
	
	
	public KeyInput(Engine engine){
		this.engine = engine;
	}
	
	public void keyPressed(KeyEvent e){
		float key = e.getKeyCode();
		
		for(int i=0;	i<engine.gameobjects.size();	i++){
		GameObjects tObject = engine.gameobjects.get(i);
		
		if(key == KeyEvent.VK_W) tObject.setVelY(-3);
		if(key == KeyEvent.VK_S) tObject.setVelY(3);
			if(key == KeyEvent.VK_A){
				
				tObject.setVelX(-3);
			
			}
		
			if(key == KeyEvent.VK_D){
				
				tObject.setVelX(3);
				
				
			}
		}
	}
	
	public void keyReleased(KeyEvent e){
		
		float key = e.getKeyCode();
		
		for(int i=0;	i<engine.gameobjects.size();	i++){
			GameObjects tObject = engine.gameobjects.get(i);
			
	
				if(key == KeyEvent.VK_W) tObject.setVelY(0);//set this object/player
				if(key == KeyEvent.VK_S) tObject.setVelY(0);
				if(key == KeyEvent.VK_A) tObject.setVelX(0);
				if(key == KeyEvent.VK_D)tObject.setVelX(0);
				
				
			
	    }
	}
}
