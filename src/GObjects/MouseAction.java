package GObjects;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseAction implements MouseListener{
	private Engine engine;
	Shot shott;
	ShotHUDPower SHUD;
	
	public static int X,Y;
	public static double rad,dab;
	
	public MouseAction(Engine engine){
		this.engine = engine;
		for(int i=0;i<engine.gameobjects.size();i++)
			if(engine.gameobjects.get(i).id==ID.Shot)
			shott = (Shot) engine.gameobjects.get(i);
		
		for(int i=0;i<engine.gameobjects.size();i++)
			if(engine.gameobjects.get(i).id==ID.SPHUD)
			SHUD = (ShotHUDPower) engine.gameobjects.get(i);		
	}
	
	public static double getX(){
		return X;
	}
	
	public static double getY(){
		return Y;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {

	//	System.out.println(dab);
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		SHUD.HEALTH=0;
		SHUD.greenValue=255;
		shott.power=0;
		shott.fire	=	true;	
		
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	//	X = e.getX();
		//Y = e.getY();
		
		shott.fire = false;
		shott.setPara();
		
		shott.shot	=	true;
		shott.shotposition = true;
		
		
	}


}
