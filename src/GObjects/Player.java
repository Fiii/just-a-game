package GObjects;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class Player extends Ally {
Engine engine;
protected BufferedImage barrel;


	public Player(int x, int y, ID id,Engine engine) {
		super(x, y, id,ResourceLoader.getImage("tank.png"));
		this.engine = engine;
		engine.addObject(new Shot(x,y,ID.Shot,engine,this));
		barrel = ResourceLoader.getImage("barrel.png");
	}



	public void tick() {
		this.X+=velX;
		this.X = GameObjects.stopper((int)X,40, 470);
		this.Y = GameObjects.stopper((int)Y,0, 450);
	}

	public void render(Graphics g) {
		g.drawImage(image,(int)X, (int)Y, null);
		
	}
	
	
	@Override
	public Rectangle getBounds() {
		// TODO Auto-generated method stub
		return new Rectangle((int)X,(int)Y,20,20);
	}		
	
	
}
