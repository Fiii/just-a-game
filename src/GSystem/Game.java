package GSystem;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import GObjects.Base;
import GObjects.Castle;
import GObjects.CastleShot;
import GObjects.Enemy;
import GObjects.EnemyTank;
import GObjects.Engine;
import GObjects.HUD;
import GObjects.HUDB;
import GObjects.KeyInput;
import GObjects.MouseAction;
import GObjects.Player;
import GObjects.ResourceLoader;
import GObjects.Shot;
import GameMenu.GMenu;
import GObjects.ID;

public class Game extends Canvas implements Runnable {
private static final long serialVersionUID = 1L;
public Thread thread;
boolean running=false;
public static int WIDTH=1000;
public static int HEIGHT=500;

GMenu GM;
Engine engine;
Shot shot;
BackGround BG;

	public Game(){
		
		new Window(WIDTH,HEIGHT,"GameMaker",this);
		GM = new GMenu(this);
		BG = new BackGround(0,0,ID.BG);
		engine = new Engine();
		
		engine.addObject(new Player(80,440,ID.Player,engine));
		engine.addObject(new Castle(850,250,ID.Castle,engine));
		
		engine.addObject(new Base(-10,380,ID.Base,engine,this));

		engine.addObject(new EnemyTank(850,435,ID.Enemy,engine));
		
		this.addKeyListener(new KeyInput(engine));
		this.addMouseListener(new MouseAction(engine));
		this.addMouseMotionListener(new Mouse());
		
		
	}
	
	
	public void start() {
		thread = new Thread(this);
		thread.start();
		running = true;
	}
	public void stop() {	
		try{
		thread.join();
		running = false;
		}catch(Exception e){
			e.printStackTrace();
		}	
	}
	public void run() {

		this.requestFocus();
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		int frames = 0;
		
		while(running){//main game loop
		long now = System.nanoTime();
		delta += (now - lastTime) /ns;
		lastTime = now;
			while(delta >= 1){
				tick();//2nd // engine tick
				delta--;		
			}
			if(running)
				render();//1st
				frames++;
				
				if(System.currentTimeMillis() - timer > 1000){
					timer += 1000;
				//System.out.println("FPS: "+frames);
				frames = 0;
				}
		  }
		stop();
		
	}
	
	public void tick() {
		engine.tick();

	}
	
	public void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if(bs==null){
			this.createBufferStrategy(3);
			return;
		}

		Graphics g = bs.getDrawGraphics();
	
		g.setColor(Color.white);
		g.fillRect(0, 0, WIDTH,	HEIGHT);
		
		BG.render(g);
		engine.render(g);

		
		g.dispose();
		bs.show();
		
	}
	
	
	public static void main(String[] args){
	new Game();
	
	}
}
