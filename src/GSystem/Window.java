package GSystem;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

public class Window extends Canvas {
private static final long serialVersionUID = 1L;
private static Game game;


	public Window(int width, int height, String title, Game game){
	
	JFrame window = new JFrame(title);
	
	
	this.game = game;

	window.setPreferredSize(new Dimension(width,height));
	window.setMinimumSize(new Dimension(width,height));
	window.setMinimumSize(new Dimension(width,height));
	
	
	window.setResizable(false);
	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	window.setLocationRelativeTo(null);
	window.add(game);
	window.setVisible(true);
	
	}
	
	public static void gamestart(){
		game.start();
	}
	
}
